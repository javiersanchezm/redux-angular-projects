import { createReducer, on } from '@ngrx/store';
import {cargarUsuario, cargarUsuarioSuccess, cargarUsuarioError } from '../actions';
import { Usuario } from 'src/app/models/usuario.model';

export interface UsuarioState {
  id: string,
  user: Usuario,
  loaded: boolean,
  loading: boolean,
  error: any
};

const usuariosInitialState: UsuarioState = {
  id: '',
  user: new Usuario(0, '', '', ''),
  loaded: false,
  loading: false,
  error: null
};

export const _usuarioReducer = createReducer( usuariosInitialState,
  on( cargarUsuario, (state, {id}) => ({...state, loading: true, id:id})),

  on( cargarUsuarioSuccess, (state, { usuario }) => ({
    ...state,
    loading: false,
    loaded: true,
    user:  {...usuario}
  })),

  on( cargarUsuarioError, (state, { payload }) => ({
    ...state,
    loading: false,
    loaded: false,
    error: {
      url: payload.url,
      name: payload.name,
      message: payload.message
    }
  }))
);


export function usuarioReducer(state:any, action:any){
  return _usuarioReducer(state, action);
}
