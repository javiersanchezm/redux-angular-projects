import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as usuariosActions from '../actions';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { UsuarioService } from 'src/app/services/usuario.service';
import { of } from 'rxjs';

@Injectable()
export class UsuariosEffects {

  //Observable que estará escuchando las acciones
  constructor( private actions$: Actions, private usuarioService: UsuarioService ){}

  //ofType indicar cual es la acción que me interesa escuchar
  cargarUsuarios$ = createEffect(
    () => this.actions$.pipe(
      ofType( usuariosActions.cargarUsuarios ),
      mergeMap(
        () => this.usuarioService.getUsers()
        .pipe(
          map( users => usuariosActions.cargarUsuariosSuccess({ usuarios: users}) ),
          catchError( err => of(usuariosActions.cargarUsuariosError({ payload: err})) )
        )
      )
    )
  );

}
