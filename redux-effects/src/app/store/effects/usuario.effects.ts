import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as usuariosActions from '../actions';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { UsuarioService } from 'src/app/services/usuario.service';
import { of } from 'rxjs';

@Injectable()
export class UsuarioEffects {

  //Observable que estará escuchando las acciones
  constructor( private actions$: Actions, private usuarioService: UsuarioService ){}

  //ofType indicar cual es la acción que me interesa escuchar
  cargarUsuario$ = createEffect(
    () => this.actions$.pipe(
      ofType( usuariosActions.cargarUsuario ),
      mergeMap(
        ( action ) => this.usuarioService.getUserById(action.id)
        .pipe(
          map( user => usuariosActions.cargarUsuarioSuccess({ usuario: user}) ),
          catchError( err => of(usuariosActions.cargarUsuarioError({ payload: err})) )
        )
      )
    )
  );

}
