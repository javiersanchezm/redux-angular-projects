// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyACa5IauAvGuUVErmHVAFKAsJq80ni0Y9g",
    authDomain: "ingre-egreso-app-pruebas.firebaseapp.com",
    projectId: "ingre-egreso-app-pruebas",
    storageBucket: "ingre-egreso-app-pruebas.appspot.com",
    messagingSenderId: "846599338103",
    appId: "1:846599338103:web:3f4a05e2266ad88c906e72"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
