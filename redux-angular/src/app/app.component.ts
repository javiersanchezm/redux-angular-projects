import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
//import { decrementar, incrementar } from './contador/contador.actions';
import * as actions from './contador/contador.actions'; //actions va a contener todo
import { AppState } from './app.reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'redux-app';

  contador!:number;

  constructor( private store: Store<AppState>){
    this.store.select('contador').subscribe( contador => this.contador = contador)
  }

  incrementar(){
    //Acción que se quiere llamar
    this.store.dispatch( actions.incrementar() );
  }

  decrementar(){
    this.store.dispatch( actions.decrementar() );
  }
}
